# Data analysis with R

## Presentation slide

- [View slides](https://hpecout.gitpages.huma-num.fr/R_presentation_EN/#/)       
- [French version](https://hpecout.gitpages.huma-num.fr/R_presentation_FR/#/)


Translated by Violaine Jurie
